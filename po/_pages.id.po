# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: team@f-droid.org\n"
"POT-Creation-Date: 2018-06-30 12:21+0200\n"
"PO-Revision-Date: 2018-05-08 19:51+0000\n"
"Last-Translator: ditokp <ditokpl@gmail.com>\n"
"Language-Team: Indonesian <https://hosted.weblate.org/projects/f-droid/website-pages/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.0-dev\n"

#. type: Title #
#: build/_pages/md/403.md
#, no-wrap
msgid "403 Forbidden"
msgstr "403 Terlarang"

#. type: Plain text
#: build/_pages/md/403.md build/_pages/md/404.md
msgid "If you think there should be something here, please [submit an issue](https://gitlab.com/fdroid/fdroid-website/issues)."
msgstr "Jika Anda pikir disini ada sesuatu, tolong [daftarkan masalah](https://gitlab.com/fdroid/fdroid-website/issues)."

#. type: Title #
#: build/_pages/md/404.md
#, no-wrap
msgid "404 Page Not Found"
msgstr "404 Halaman Tidak Ditemukan"

#. type: Title #
#: build/_pages/md/about.md
#, no-wrap
msgid "About"
msgstr "Tentang"

#. type: Plain text
#: build/_pages/md/about.md
msgid "F-Droid is a robot with a passion for Free and Open Source (FOSS) software on the Android platform. On this site you’ll find a repository of FOSS apps, along with an Android client to perform installations and updates, and news, reviews and other features covering all things Android and software-freedom related."
msgstr "F-Droid adalah robot dengan semangat untuk Aplikasi Gratis dan Kode Sumber Terbuka (FOSS) di Android. Di situs ini Anda akan menemukan repositori dari aplikasi FOSS, beserta dengan client Android untuk melakukan pemasangan dan pembaharuan, berita, mengulas serta fitur lainnya tentang Android dan kemerdekaan aplikasi."

#. type: Plain text
#: build/_pages/md/about.md
msgid "F-Droid is operated by F-Droid Limited, a non-profit organisation registered in England (no. 8420676)."
msgstr "F-Droid di operasikan oleh F-Droid Limited, organisasi non-profit yang teregistrasi di Inggris (no. 8420676)."

#. type: Title ####
#: build/_pages/md/about.md
#, no-wrap
msgid "Contact"
msgstr "Kontak"

#. type: Plain text
#: build/_pages/md/about.md
msgid "You can discuss F-Droid or get help on the [forum](https://forum.f-droid.org) and IRC in the [#fdroid channel](https://webchat.freenode.net/?channels=%23fdroid) on freenode. The discussions on IRC are logged on [BotBot.me](https://botbot.me/freenode/fdroid).  You can send email at [team@f-droid.org](mailto:team@f-droid.org), but messages are much more likely to be answered on the forum or #fdroid channel. If you want to help, you can [join us]({{ \"/contribute/\" | prepend: site.baseurl }})."
msgstr "Anda bisa berdiskusi tentang F-Droid atau meminta bantuan di [forum](https://forum.f-droid.org) dan IRC di [channel #fdroid](https://webchat.freenode.net/?channels=%23fdroid) di freenode. Diskusi di IRC tercatat di [BotBot.me](https://botbot.me/freenode/fdroid). Anda dapat mengirim surel ke [team@f-droid.org](mailto:team@f-droid.org), tapi pesan-pesan lebih sering dibalas di forum atau channel #fdroid. Jika Anda ingin membantu, Anda bisa [bergabung]({{ \"/contribute/\" | prepend: site.baseurl }})."

#. type: Title ###
#: build/_pages/md/about.md
#, no-wrap
msgid "License"
msgstr "Lisensi"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "The source code for this site and the content produced by the F-Droid community are a availble under the [GNU Affero General Public License version 3 or later (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.html)."
msgstr "Kode sumber dari situs ini dan konten yang di produksi oleh komunitas F-Droid tersedia di bawah [GNU Affero General Public License versi 3 atau setelahnya (AGPLv3+)](https://www.gnu.org/licenses/agpl-3.0.html)."

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "The content of the news section and the F-Droid logo are also available under [Creative Commons Attribution-ShareAlike 3.0 License (CC-BY-SA-3.0)](http://creativecommons.org/licenses/by-sa/3.0/) or [GNU General Public License version 3 or later (GPLv3+)](https://www.gnu.org/licenses/gpl-3.0.html)."
msgstr "Konten yang ada di bagian berita dan logo F-Droid juga tersedia dibawah [Lisensi Creative Commons Attribution-ShareAlike 3.0 (CC-BY-SA-3.0)](http://creativecommons.org/licenses/by-sa/3.0/) atau [GNU General Public License version 3 or later (GPLv3+)](https://www.gnu.org/licenses/gpl-3.0.html)."

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "The descriptive text and graphics for each app have a specific license from that project.  Free software should also have free descriptions and graphics."
msgstr "Deskripsi teks dan gambar dari tiap aplikasi mempunyai lisensi sendiri. Aplikasi gratis juga mempunyai deskripsi dan gambar secara gratis."

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "The F-Droid logo was created by William Theaker. Updates to that, and additional artwork, are by Robert Martinez."
msgstr "Logo F-Droid dibuat oleh William Theaker. Pembaharuan dan beberapa tambahan oleh Robert Martinez."

#. type: Title ###
#: build/_pages/md/about.md
#, no-wrap
msgid "Terms, etc."
msgstr "Terms, dll."

#. type: Plain text
#: build/_pages/md/about.md
msgid "F-Droid is a non-profit [volunteer]({{ \"/contribute/\" | prepend: site.baseurl }}) project. Although every effort is made to ensure that everything in the repository is safe to install, you use it AT YOUR OWN RISK. Wherever possible, applications in the repository are built from source, and that source code is checked for potential security or privacy issues. This checking is far from exhaustive though, and there are no guarantees."
msgstr "F-Droid adalah proyek non-profit [sukarela]({{ \"/contribute/\" | prepend: site.baseurl }}). Meskipun setiap upaya dilakukan untuk memastikan bahwa segala sesuatu dalam repositori aman untuk dipasang, Anda menggunakannya dengan RESIKO ANDA SENDIRI. Jika memungkinkan, aplikasi di dalam repositori dibuat dari awal, dan kode sumber tersebut diperiksa untuk melihat potensi masalah keamanan atau privasi. Pemeriksaan ini jauh dari lengkap, dan tidak ada jaminan."

#. type: Plain text
#: build/_pages/md/about.md
msgid "F-Droid respects your privacy. We don’t track you, or your device. We don’t track what you install. You don’t need an account to use the client, and it sends no additional identifying data when talking to our web server other than its version number. We don’t even allow you to install other applications from the repository that track you, unless you first enable ‘Tracking’ in the AntiFeatures section of preferences. Any personal data you decide to give us (e.g. your email address when registering for an account to post on the forum) goes no further than us, and will not be used for anything other than allowing you to maintain your account."
msgstr "F-Droid menghormati privasi Anda. Kami tidak melacak Anda ataupun perangkat Anda. Kami tidak melacak apa yang di pasang. Anda tidak butuh akun untuk menggunakan clientnya dan clientnya tidak mengirim data pengidentifikasian tambahan ketika berbicara kepada server web kami apapun selain nomor versi. Kami bahkan tidak memperbolehkan Anda untuk memasang aplikasi lain dari repositori yang melacak Anda, kecuali kalau Anda mengaktifkan 'Pelacakan' di bagian AntiFitur di pengaturan. Data pribadi apapun yang Anda pilih untuk diberikan kepada kami (contohnya alamat surel Anda ketika mendaftarkan akun untuk memposting di forum) tidak akan pergi lebih jauh daripada kami dan tidak akan digunakan untuk apapun selain untuk memperbolehkan Anda untuk memelihara akun Anda."

#. type: Title ###
#: build/_pages/md/about.md
#, no-wrap
msgid "Contributors"
msgstr "Kontributor"

#. type: Plain text
#: build/_pages/md/about.md
msgid "The F-Droid project was founded in 2010 by Ciaran Gultnieks, and is brought to you by at least the following people:"
msgstr "Proyek F-Droid didirikan pada tahun 2010 oleh Ciaran Gultnieks dan dibawa kehadapan Anda oleh beberapa orang berikut ini:"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Nico Alt"
msgstr "Nico Alt"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Laura Arjona"
msgstr "Laura Arjona"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Ben Arnold"
msgstr "Ben Arnold"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Michele Azzolari"
msgstr "Michele Azzolari"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Danial Behzadi"
msgstr "Danial Behzadi"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Jan Berkel"
msgstr "Jan Berkel"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "David Black"
msgstr "David Black"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Ruslan Boitsov"
msgstr "Ruslan Boitsov"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Jan C Borchardt"
msgstr "Jan C Borchardt"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Valerio Bozzolan"
msgstr "Valerio Bozzolan"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Dan Bravender"
msgstr "Dan Bravender"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Cédric Cabessa"
msgstr "Cédric Cabessa"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Kevin Cernekee"
msgstr "Kevin Cernekee"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Felix Edelmann"
msgstr "Felix Edelmann"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Kevin Everets"
msgstr "Kevin Everets"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Alberto A. Fuentes"
msgstr "Alberto A. Fuentes"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Torsten Grote"
msgstr "Torsten Grote"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Ciaran Gultnieks"
msgstr "Ciaran Gultnieks"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Tias Guns"
msgstr "Tias Guns"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Michael Haas"
msgstr "Michael Haas"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Stefan Handschuh"
msgstr "Stefan Handschuh"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Marcus Hoffmann"
msgstr "Marcus Hoffmann"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Kees Hulberts"
msgstr "Kees Hulberts"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Ayron Jungren"
msgstr "Ayron Jungren"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "João Fernando C. Júnior"
msgstr "João Fernando C. Júnior"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Neo Kolokotronis"
msgstr "Neo Kolokotronis"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Nikita Kozlov"
msgstr "Nikita Kozlov"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Matt Kraai"
msgstr "Matt Kraai"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Daniel Kraft"
msgstr "Daniel Kraft"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Boris Kraut"
msgstr "Boris Kraut"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Tobias Kuban"
msgstr "Tobias Kuban"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Mariotaku Lee"
msgstr "Mariotaku Lee"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Leonardo De Luca"
msgstr "Leonardo De Luca"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Greg Lyle"
msgstr "Greg Lyle"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Daniel Marti"
msgstr "Daniel Marti"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Robert Martinez"
msgstr "Robert Martinez"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Nat Meysenburg"
msgstr "Nat Meysenburg"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Steven McDonald"
msgstr "Steven McDonald"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Andrew McMillan"
msgstr "Andrew McMillan"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Andrew Nayenko"
msgstr "Andrew Nayenko"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Alex Oberhauser"
msgstr "Alex Oberhauser"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Michael Pöhn"
msgstr "Michael Pöhn"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Nicolas Raoul"
msgstr "Nicolas Raoul"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Fabian Rodriguez"
msgstr "Fabian Rodriguez"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Pierre Rudloff"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Florian Schmaus"
msgstr "Florian Schmaus"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Dominik Schürmann"
msgstr "Dominik Schürmann"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Hans-Emil Skogh"
msgstr "Hans-Emil Skogh"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Paul Sokolovsky"
msgstr "Paul Sokolovsky"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Graziano Sorbaioli"
msgstr "Graziano Sorbaioli"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Hans-Christoph Steiner"
msgstr "Hans-Christoph Steiner"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "David Sterry"
msgstr "David Sterry"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Peter Serwylo"
msgstr "Peter Serwylo"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "John Sullivan"
msgstr "John Sullivan"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Antonio Tapiador"
msgstr "Antonio Tapiador"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "William Theaker"
msgstr "William Theaker"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Henrik Tunedal"
msgstr "Henrik Tunedal"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Ivo Ugrina"
msgstr "Ivo Ugrina"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Matthias Weiler"
msgstr "Matthias Weiler"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Nutchanon Wetchasit"
msgstr "Nutchanon Wetchasit"

#. type: Bullet: '* '
#: build/_pages/md/about.md
msgid "Philipp Wollschlegel"
msgstr "Philipp Wollschlegel"

#. type: Plain text
#: build/_pages/md/about.md
msgid "If your name is missing, it’s a mistake and you should [add yourself to the list](https://gitlab.com/fdroid/fdroid-website/blob/master/_pages/about.md)! Keep it sorted by surname, please."
msgstr "Jika nama Anda menghilang, pasti ada kesalahan dan Anda dapat [menambah nama Anda ke daftar](https://gitlab.com/fdroid/fdroid-website/blob/master/_pages/about.md)! Tolong tetap di urutkan sesuai nama keluarga."

#. type: Plain text
#: build/_pages/md/about.md
msgid "The original F-Droid client app was based on the Aptoide app developed by Roberto Jacinto."
msgstr "Aplikasi client F-Droid yang asli berdasarkan aplikasi Aptoide yang dikembangkan oleh Roberto Jacinto."

#. type: Title #
#: build/_pages/md/contribute.md
#, no-wrap
msgid "Contribute"
msgstr "Berkontribusi"

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "The project is entirely developed and maintained by volunteers. You can help:"
msgstr "Seluruh proyek dikembangkan dan diperbaiki secara sukarela. Anda bisa membantu:"

#. type: Title ###
#: build/_pages/md/contribute.md
#, no-wrap
msgid "Report Problems"
msgstr "Melaporkan Masalah"

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "If you experience problems with the site or client software, you can report them in the issue tracker (see below), or discuss them in the [Forum](https://forum.f-droid.org/) or on IRC (#fdroid on freenode)."
msgstr "Jika Anda merasakan masalah dengan situs atau aplikasi client, Anda dapat melaporkannya ke pelacak masalah (lihat di bawah), atau mendiskusikannya di [Forum](https://forum.f-droid.org/) atau di IRC (#fdroid on freenode)."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "For reporting issues, choose an appropriate tracker from the following:"
msgstr "Untuk melaporkan masalah, pilih pelacak yang sesuai dari daftar berikut:"

#. type: Bullet: '* '
#: build/_pages/md/contribute.md
msgid "[F-Droid client app](https://gitlab.com/fdroid/fdroidclient/issues)"
msgstr "[Aplikasi client F-Droid](https://gitlab.com/fdroid/fdroidclient/issues)"

#. type: Bullet: '* '
#: build/_pages/md/contribute.md
msgid "[Server and build tools](https://gitlab.com/fdroid/fdroidserver/issues)"
msgstr "[Server dan build tools](https://gitlab.com/fdroid/fdroidserver/issues)"

#. type: Bullet: '* '
#: build/_pages/md/contribute.md
msgid "[Official F-Droid repository metadata](https://gitlab.com/fdroid/fdroiddata/issues)"
msgstr "[Metadata repositori F-Droid resmi](https://gitlab.com/fdroid/fdroiddata/issues)"

#. type: Title ###
#: build/_pages/md/contribute.md
#, no-wrap
msgid "Submit Applications"
msgstr "Kirim Aplikasi"

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "If you see an application missing from the repository (after reading the [inclusion policy](../docs/Inclusion_Policy)), please feel free to submit it via the dedicated [Requests For Packaging](https://gitlab.com/fdroid/rfp/issues) issue tracker."
msgstr "Jika Anda melihat aplikasi yang menghilang dari repository (setelah membaca [kebijakan pencantuman](../docs/Inclusion_Policy)), silahkan mengirimkannya melalui pelacakan masalah untuk [Permintaan untuk Kemasan](https://gitlab.com/fdroid/rfp/issues)."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "If you have the technical skills required, you can also put together the relevant metadata and [submit that via the F-Droid Data repository](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md), which will drastically speed up the inclusion of the application."
msgstr "Jika Anda mempunyai kemampuan teknis yang diperlukan, Anda bisa memasukkan metada yang relevan dan [mengirimkannya melalui repositori Data F-Droid](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md), yang secara cepat akan mempercepat masuknya aplikasi."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "The same applies for helping to build newer versions of applications."
msgstr "Hal itu juga berlaku untuk membantu membuat versi baru dari aplikasi."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "Further information can be found in [the documentation](../docs), or by asking [on IRC](https://webchat.freenode.net/?channels=%23fdroid) (#fdroid on freenode)."
msgstr "Informasi lebih lanjut bisa ditemukan di [dokumentasi](../docs), atau bertanya [di IRC](https://webchat.freenode.net/?channels=%23fdroid) (#fdroid on freenode)."

#. type: Title ###
#: build/_pages/md/contribute.md
#, no-wrap
msgid "Translate"
msgstr "Terjemahkan"

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "The client application is available in many languages. Should yours not be among those, or be in need of updates or improvement, please create an account and use the [translation system](https://hosted.weblate.org/projects/f-droid/) to make your changes."
msgstr "Aplikasi client tersedia di banyak bahasa, Jika bahasa Anda tidak berada di antaranya, atau membutuhkan pembaruan atau peningkatan, silakan buat akun dan gunakan [sistem terjemahan](https://hosted.weblate.org/projects/f-droid/) untuk membuat perubahan."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "Start with the overview of [Translation and Localization](../docs/Translation_and_Localization).  There’s also a [dedicated forum section](https://forum.f-droid.org/c/translation) for translation related discussions."
msgstr "Dimulai dengan gambaran dari [Menerjemahkan dan Melokalisasi](../docs/Translation_and_Localization). Ada juga [bagian dari forum](https://forum.f-droid.org/c/translation) untuk berdiskusi tentang terjemahan."

#. type: Title ###
#: build/_pages/md/contribute.md
#, no-wrap
msgid "Help with Development"
msgstr "Bantuan Pengembangan"

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "There are four git repositories hosted at GitLab – one for the [Android client application](https://gitlab.com/fdroid/fdroidclient), one for the [server tools](https://gitlab.com/fdroid/fdroidserver) for running a repository and building/installing applications locally, and one for the associated [metadata files for applications in the main F-Droid repository](https://gitlab.com/fdroid/fdroiddata).  The last repo is handling [website and documentation](https://gitlab.com/fdroid/fdroid-website).  The easiest way to contribute to development is to make clones of these projects and submit merge requests.  If you are making large changes, it would be good to discuss them on IRC or in the forum first, to ensure they fit with the direction of the project, and do not clash with or duplicate work already in development."
msgstr "Ada empat repositori git yang di taruh di Gitlab - yang pertama untuk [Aplikasi client Android](https://gitlab.com/fdroid/fdroidclient), kedua untuk [server tools](https://gitlab.com/fdroid/fdroidserver) untuk menjalankan repositori dan membuat/memasang aplikasi secara lokal, dan yang ketiga untuk file terkait [metadata untuk aplikasi di repositori F-Droid utama](https://gitlab.com/fdroid/fdroiddata).  Repositori terakhir menangani [situs web dan dokumentasi](https://gitlab.com/fdroid/fdroid-website). Cara tergampang untuk berkontribusi untuk membantu pengembangan adalah menyalin proyek ini dan mengirimkan permintaan penggabungan. Jika Anda membuat perubahan yang besar, lebih baik didiskusikan dahulu di IRC atau di forum untuk memastikan mereka sesuai arah direksi dari proyek, dan jangan berbenturan dengan atau menduplikasi pekerjaan yang sudah dalam pengembangan."

#. type: Plain text
#: build/_pages/md/contribute.md
msgid "For working with the server and data projects, it’s a good idea to read [the manual]({{ \"/docs/\" | prepend: site.baseurl }})."
msgstr "Untuk bekerja dengan server dan data proyek, itu ide yang bagus untuk membaca [manual]({{ \"/docs/\" | prepend: site.baseurl }})."

#. type: Title #
#: build/_pages/md/docs.md
#, no-wrap
msgid "Docs"
msgstr "Dokumen"

#. type: Plain text
#: build/_pages/md/docs.md
msgid "F-Droid is both a repository of verified free software Android apps as well as a whole \"app store kit\", providing all the tools needed to setup and run an app store. It is a community-run free software project developed by a wide range of contributors. It also includes complete build and release tools for managing the process of turning app source code into published builds."
msgstr ""

#. type: Plain text
#: build/_pages/md/docs.md
msgid "The F-Droid server tools provide various scripts and tools that are used to maintain the main F-Droid application repository. You can use these same tools to create your own additional or alternative repository for publishing, or to assist in creating, testing and submitting metadata to the main repository."
msgstr ""

#. type: Title ###
#: build/_pages/md/docs.md
#, no-wrap
msgid "Getting Started"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[FAQ - General](FAQ_-_General) Frequently Asked Questions about F-Droid"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[FAQ - Client](FAQ_-_Client) Frequently Asked Questions about how to use the F-Droid app in Android."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Anti-Features](https://f-droid.org/wiki/page/Antifeatures) What they are, what they mean, and what apps have them."
msgstr ""

#. type: Title ###
#: build/_pages/md/docs.md
#, no-wrap
msgid "More Information"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[How to Help](How_to_Help) Different ways anyone can contribute to the F-Droid project."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Known Repositories](https://forum.f-droid.org/t/721) A list that tries to keep track of known F-Droid compatible repositories."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Release Channels and Signing Keys](Release_Channels_and_Signing_Keys) These are the various channels that F-Droid software are released on, with info to verify them based on signing keys."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Security Model](Security_Model) A brief explanation of how F-Droid delivers software to users securely."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Translation and Localization](Translation_and_Localization) How all the parts of F-Droid are localized."
msgstr ""

#. type: Title ###
#: build/_pages/md/docs.md
#, no-wrap
msgid "Developers"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[FAQ - App Developers](FAQ_-_App_Developers) Frequently Asked Questions about F-Droid"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Build Metadata Reference](Build_Metadata_Reference) All about the build \"recipes\""
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Submitting to F-Droid: Quick Start Guide](Submitting_to_F-Droid_Quick_Start_Guide) Add an app to f-droid.org"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Update Processing](Update_Processing) How updates get detected and added."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Inclusion Policy](Inclusion_Policy) & [Inclusion How-To](Inclusion_How-To)  Guidelines for how to request the inclusion of a new app."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Importing Applications](Importing_Applications) Using `fdroid import` to build a new project."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Building Applications](Building_Applications) Using `fdroid build` to build an app."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[All About Descriptions, Graphics, and Screenshots](All_About_Descriptions_Graphics_and_Screenshots) Showing off your app in F-Droid."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Publishing Nightly Builds](Publishing_Nightly_Builds) Make an automated repo of nightly builds of your app."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Installing the Server and Repo Tools](Installing_the_Server_and_Repo_Tools) How to install fdroidserver, the tools for running repos and build servers."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Setup an F-Droid App Repo](Setup_an_F-Droid_App_Repo) How to setup your own repository of apps that users can easily add to their F-Droid client to get your apps directly from the source."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Build Server Setup](Build_Server_Setup) How to setup the complete f-droid.org build setup on your own machine."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Signing Process](Signing_Process) How to setup cryptographic signatures with F-Droid tools"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Reproducible Builds](Reproducible_Builds) How to use the F-Droid tools to make reproducible builds easy."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Building a Signing Server](Building_a_Signing_Server) For higher security, isolate the signing keys from the rest."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Verification Server](Verification_Server) Setting up a server to verify app builds are correct."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Whitelabel Builds](Whitelabel_Builds) custom builds of F-Droid"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Privileged Extension](https://gitlab.com/fdroid/privileged-extension/#f-droid-privileged-extension) How to use F-Droid Privileged Extension as a user and rom developer"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Deploying the Website](Deploying_the_Website) How [fdroid-website.git](https://gitlab.com/fdroid/fdroid-website) becomes [f-droid.org](https://f-droid.org)"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Release Process](Release_Process) All the steps needed to make official release of F-Droid projects."
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Maintaining the Forum](Maintaining_the_Forum) Useful information for administrators of [F-Droid's forum](https://forum.f-droid.org)"
msgstr ""

#. type: Bullet: '* '
#: build/_pages/md/docs.md
msgid "[Work in Progress](https://f-droid.org/wiki/page/Work_in_Progress) Feel free to add your own topics and link to others."
msgstr ""

#. type: Title #
#: build/_pages/md/issues.md
#, no-wrap
msgid "Issues"
msgstr ""

#. type: Plain text
#: build/_pages/md/issues.md
#, no-wrap
msgid ""
"* [Client](https://gitlab.com/fdroid/fdroidclient/issues) issue tracker: If you’re having a problem with the F-Droid client application,\n"
"please check if we already know about it, and/or report it in this issue tracker.\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/issues.md
#, no-wrap
msgid ""
"* [RFP](https://gitlab.com/fdroid/rfp/issues/new) tracker: To request a new app be added, submit a \"Request For Packaging\" on the\n"
"rfp tracker.\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/issues.md
#, no-wrap
msgid ""
"* [Data](https://gitlab.com/fdroid/fdroiddata/issues) issue tracker: For problems relating to the contents of the repository,\n"
"such as missing or outdated applications,\n"
"please use this issue tracker.\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/issues.md
#, no-wrap
msgid ""
"* [Server](https://gitlab.com/fdroid/fdroidserver/issues) issue tracker: For the server tools (used for building apps yourself,\n"
"or running your own repo), please use this issue tracker.\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/issues.md
#, no-wrap
msgid ""
"* [Website](https://gitlab.com/fdroid/fdroid-website/issues) issue tracker: For issues relating to this web site, you can use\n"
"this issue tracker.\n"
msgstr ""

#. type: Title #
#: build/_pages/md/repomaker.md
#, no-wrap
msgid "Repomaker"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
msgid "F-Droid.org is just a repo out of hundreds of repos created by individuals all around the globe. With the tools of F-Droid, everyone can create their own repo. So whether you are a musician who wants to publish their music or a developer who wants to serve nightly builds of their app, you are free to create your own repo and share it with other people independently of F-Droid.org."
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
msgid "In the past, creating a repo has been difficult because you had to have knowledge on the command line, needed to edit text files to edit your packages' store details and had to paste screenshots in a special system of directories to have them served well inside the F-Droid app."
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
msgid "This all got easier now: with Repomaker, you are able to create your own repo and do not need to have any special knowledge to do so. More information on how to install Repomaker coming soon!"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
#, no-wrap
msgid ""
"[![]({{ site.baseurl }}/assets/repomaker-screenshots/repo-details.png)]({{ site.baseurl }}/assets/repomaker-screenshots/repo-details.png)<br/>\n"
"*Your repo can serve all types of media*\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
#, no-wrap
msgid ""
"[![]({{ site.baseurl }}/assets/repomaker-screenshots/package-details.png)]({{ site.baseurl }}/assets/repomaker-screenshots/package-details.png)<br/>\n"
"*Editing your package store details never was easier*\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
#, no-wrap
msgid ""
"[![]({{ site.baseurl }}/assets/repomaker-screenshots/create-repo.png)]({{ site.baseurl }}/assets/repomaker-screenshots/create-repo.png)<br/>\n"
"*Creating a repo just takes you two clicks*\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
#, no-wrap
msgid ""
"[![]({{ site.baseurl }}/assets/repomaker-screenshots/add-storage.png)]({{ site.baseurl }}/assets/repomaker-screenshots/add-storage.png)<br/>\n"
"*You have many possibilities where to upload your repo*\n"
msgstr ""

#. type: Plain text
#: build/_pages/md/repomaker.md
msgid "Like any other project of F-Droid, Repomaker is free and open source software. You can find the source code and its license on [GitLab.com](https://gitlab.com/fdroid/repomaker)."
msgstr ""
